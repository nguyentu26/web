package models;

import play.data.validation.ValidationError;

import java.util.ArrayList;
import java.util.List;

public class FormAcc {
    public String email = "";

    public String password = "";

    public FormAcc() {
    }

    public List<ValidationError> validationErrors() {
        List<ValidationError> errors = new ArrayList<>();
        if (!UserDB.isValid(email, password)) {
            errors.add(new ValidationError("email", ""));
            errors.add(new ValidationError("password", ""));
        }

        return (errors.size() > 0) ? errors : null;
    }
}
