package controllers;

import models.FormAcc;
import play.*;
import play.data.Form;
import play.mvc.*;

import views.html.*;
import views.html.Login;

public class Application extends Controller {
    public Result index() {

        return ok(index.render("Your new application is ready."));
    }

    public Result login() {
        return ok(Login.apply());

    }

    public Result postLogin() {
        Form<FormAcc> formData = Form.form(FormAcc.class).bindFromRequest();
        System.out.println(formData.data().get("email"));
        if (formData.data().get("email").equals("admin@gmail.com") && formData.data().get("password").equals("admin")) {       // email/password OK, so now we set the session variable and only go to authenticated pages.
            return redirect(routes.Application.login());
        } else {
            return ok(index.render("Your new application is ready."));
        }
    }

    public Result logout() {
        session().clear();
        return redirect(routes.Application.index());
    }
}
