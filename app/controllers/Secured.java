package controllers;

import models.UserAcc;
import models.UserDB;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;

import play.mvc.Http.Context;

public class Secured extends Security.Authenticator {
    @Override
    public String getUsername(Http.Context ctx) {
        return ctx.session().get("email");
    }


    @Override
    public Result onUnauthorized(Http.Context ctx) {
        return redirect(routes.Application.postLogin());
    }

    public static String getUser(Context ctx) {
        return ctx.session().get("email");
    }

    /**
     * True if there is a logged in user, false otherwise.
     *
     * @param ctx The context.
     * @return True if user is logged in.
     */
    public static boolean isLoggedIn(Context ctx) {
        return (getUser(ctx) != null);
    }

    /**
     * Return the UserInfo of the logged in user, or null if no user is logged in.
     *
     * @param ctx The context.
     * @return The UserInfo, or null.
     */
    public static UserAcc getUserInfo(Context ctx) {
        return (isLoggedIn(ctx) ? UserDB.getUser(getUser(ctx)) : null);
    }
}
